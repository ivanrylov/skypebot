'use strict';

var https = require('https');
var async = require('async');
var cheerio = require('cheerio');
var querystring = require('querystring');
var url = require('url');
var ServerSwitcher = require('./server.switcher');
var whereSkypeTokenHost = 'login.skype.com';
var whereSkypeTokenPath = '/login?client_id=578134&redirect_uri=' + encodeURIComponent('https://web.skype.com/');
var whereSkypeApi = [
  'client-s.gateway.messenger.live.com',
  'bn1-client-s.gateway.messenger.live.com'
];
var registrationTokenPath = '/v1/users/ME/endpoints';
var getMessagesPath = '/v1/users/ME/endpoints/SELF/subscriptions/0/poll';
var setSubscriptionsPath = '/v1/users/ME/endpoints/SELF/subscriptions';
var SkypeError = require('./../errors/skype');
var maxRequestTries = 2;

function SkypeApi(login, password) {
  this.login = login;
  this.password = password;
  this.skypeToken = null;
  this.registrationToken = null;
  this.server = new ServerSwitcher(whereSkypeApi);
  this.requestTries = 0;
  this.skypeid = null;
  this.tokenExpTime = null;
}

SkypeApi.prototype.getSkypeToken = function(cb) {
  var self = this;

  async.waterfall([
    // Получаем форму авторизации
    function(next) {
      https.get({
        host: whereSkypeTokenHost,
        path: whereSkypeTokenPath
      }, function(res) {
        var body = '';
        res.on('data', function(chunk) {
          body += chunk;
        });

        res.on('end', function() {
          next(null, body);
        });
      });
    },
    // Парсим нужные данные
    function(body, next) {
      var $ = cheerio.load(body);
      var loginForm = $('#loginForm');
      var clientId = loginForm.find('input[name="client_id"]').val();
      var etm = loginForm.find('input[name="etm"]').val();
      var pie = loginForm.find('input[name="pie"]').val();

      next(null, clientId, etm, pie);
    },
    // Получаем страницу с заветным skypeToken
    function(clientId, etm, pie, next) {
      var req = https.request({
        host: whereSkypeTokenHost,
        path: whereSkypeTokenPath,
        method: 'POST'
      }, function(res) {
        var body = '';
        res.on('data', function(chunk) {
          body += chunk;
        });

        res.on('end', function() {
          next(null, body);
        });
      });

      req.write(querystring.stringify({
        username: self.login,
        password: self.password,
        client_id: clientId,
        etm: etm,
        pie: pie
      }));
      req.end();
    },
    // Парсим skypeToken
    function(body, next) {
      var $ = cheerio.load(body);
      var redirectForm = $('#redirectForm');
      var skypeToken = redirectForm.find('input[name="skypetoken"]').val();
      var payloadData = JSON.parse(new Buffer(skypeToken.split('.')[1], 'base64').toString('ascii'));

      self.skypeToken = skypeToken;
      self.skypeid = payloadData.skypeid;
      self.tokenExpTime = (payloadData.exp - payloadData.iat) * 1000 + new Date().getTime();

      next(null);
    }
  ], cb);
};

SkypeApi.prototype.getSkypeId = function() {
  return this.skypeid;
};

SkypeApi.prototype.isMe = function(fromUrl) {
  return url.parse(fromUrl).pathname === ('/v1/users/ME/contacts/8:' + this.getSkypeId());
};

SkypeApi.prototype.getRegistrationToken = function(cb) {
  var self = this;

  if (!self.skypeToken) {
    return cb(new Error('Maybe login or password is incorrect'));
  }

  var req = https.request({
    host: self.server.get(),
    path: registrationTokenPath,
    method: 'POST',
    headers: {
      Authentication: 'skypetoken=' + self.skypeToken,
      'Content-Type': 'application/json'
    }
  }, function(res) {
    switch(res.statusCode) {
      case 201:
        var registrationToken = res.headers['set-registrationtoken'];
        self.registrationToken = registrationToken.split(';')[0];
        return cb(null);

      case 301:
        if (res.headers.location) {
          self.server.set(url.parse(res.headers.location).hostname);
          return self.getRegistrationToken(cb);
        }
        break;
    }

    cb(new Error('Registration fail'));
  });

  req.write('{}');
  req.end();
};

SkypeApi.prototype.doLogin = function(cb) {
  var self = this;
  self.loginTries++;

  async.waterfall([
    function(next) {
      self.getSkypeToken(next);
    },
    function(next) {
      self.getRegistrationToken(next);
    },
    function(next) {
      self.setSubscriptions(next);
    }
  ], cb);
};

SkypeApi.prototype.doRequestWithRegToken = function(method, path, reqBody, cb) {
  var self = this;

  if (!self.registrationToken) {
    return cb(new Error('RegistrationToken was not received'));
  }

  if (self.tokenExpTime - 60000 < new Date().getTime()) {
    return requestWithRelogin(cb);
  }

  var req = https.request({
    host: self.server.get(),
    path: path,
    method: method,
    headers: {
      Accept: 'application/json, text/javascript',
      RegistrationToken: self.registrationToken,
      'Content-Type': 'application/json'
    }
  }, function(res) {
    var body = '';
    res.on('data', function(chunk) {
      body += chunk;
    });

    res.on('end', function() {
      if (!body) {
        return cb(null);
      }

      if (res.statusCode === 403) {
        return requestWithRelogin(cb);
      }

      var resBody = JSON.parse(body);
      if (resBody.errorCode) {
        if (resBody.errorCode === 729 && ++self.requestTries < maxRequestTries) {
          self.server.next();
          return self.doRequestWithRegToken(method, path, reqBody, cb);
        }
        return cb(new SkypeError(resBody.message || ('code = ' + resBody.errorCode)));
      }
      cb(null, resBody);
    });
  });

  if (reqBody) { req.write(JSON.stringify(reqBody)); }
  req.end();

  function requestWithRelogin(cb) {
    self.doLogin(function(err) {
      if (err) { console.log(err); }
      self.doRequestWithRegToken(method, path, reqBody, cb);
    });
  }
};

SkypeApi.prototype.setSubscriptions = function(cb) {
  this.doRequestWithRegToken('POST', setSubscriptionsPath, {
    channelType: 'httpLongPoll',
    template: 'raw',
    interestedResources: [
      '/v1/users/ME/conversations/ALL/properties',
      '/v1/users/ME/conversations/ALL/messages',
      '/v1/users/ME/contacts/ALL',
      '/v1/threads/ALL'
    ]
  }, cb);
};

SkypeApi.prototype.getMessages = function(cb) {
  this.doRequestWithRegToken('POST', getMessagesPath, null, cb);
};

SkypeApi.prototype.writeResponse = function(fromMessage, messageText, cb) {
  var path = null;
  if (fromMessage.resource.messages) {
    path = url.parse(fromMessage.resource.messages).pathname;
  } else {
    path = url.parse(fromMessage.resource.conversationLink).pathname + '/messages';
  }

  this.doRequestWithRegToken('POST', path, {
    'content': messageText,
    'contenttype': 'text',
    'messagetype': 'RichText'
    }, function(err) {
      cb(err);
    }
  );
};

module.exports = SkypeApi;

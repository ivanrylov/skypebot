'use strict';

function ServerSwitcher(servers) {
  this.servers = servers;
  this.currentIdx = 0;
  this.currentSrv = servers[0];
}

ServerSwitcher.prototype.next = function() {
  if (!this.servers[++this.currentIdx]) {
    this.currentIdx = 0;
  }
  this.currentSrv = this.servers[this.currentIdx];

  return this.get();
};

ServerSwitcher.prototype.get = function() {
  return this.currentSrv;
};

ServerSwitcher.prototype.set = function(str) {
  this.currentSrv = str;
};

module.exports = ServerSwitcher;

'use strict';

var config = require('./../config');
var moduleConfig = config.modules.jira;

// %bot jira: issue {issueName} status
// %bot jira: issue {issueName} status to {statusName}

module.exports = function(skypeApi, arg, message) {
  var m;
  m = arg.match(/^issue\s([\w\-\d]+)\sstatus$/i);
  if (m) {
    return doRequest('GET', '/rest/api/2/issue/' + m[1], null, function(err, res) {
      skypeApi.writeResponse(message, '<i>-></i> ' + res.fields.status.name, function(err) {
        if (err) { console.log(err); }
      });
    });
  }

  m = arg.match(/^issue\s([\w\-\d]+)\sstatus\sto\s([\w\s]+)$/i);
  if (m) {
    return doRequest('GET', '/rest/api/2/issue/' + m[1] + '/transitions', null, function(err, res) {
      var statuses = {};
      res.transitions.forEach(function(i) {
        statuses[i.to.name] = i.id;
      });

      var statusTexts = Object.keys(statuses);

      if (statusTexts.indexOf(m[2]) >= 0) {
        doRequest('POST', '/rest/api/2/issue/' + m[1] + '/transitions', {
          transition: {
            id: statuses[m[2]]
          }
        }, function(err) {
          if (err) {
            if (err.errorMessages) {
              skypeApi.writeResponse(message, '<i>-> Error messages from jira: ' + err.errorMessages.join(', ') + '</i>', function(err) {
                if (err) { console.log(err); }
              });
            }
          } else {
            skypeApi.writeResponse(message, '<i>-> OK</i>', function(err) {
              if (err) { console.log(err); }
            });
          }
        });
      } else {
        skypeApi.writeResponse(message, '<i>-> Error: Incorrect status. Avalaible statuses for this issue: ' + statusTexts.join(', ') + '</i>', function(err) {
          if (err) { console.log(err); }
        });
      }
    });
  }

  // m = arg.match(/^[issues|tasks]\sfor\sme$/i);
};

function doRequest(method, path, reqBody, cb) {
  var req = require(moduleConfig.ssl ? 'https' : 'http').request({
    host: moduleConfig.hostname,
    path: path,
    method: method,
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Basic ' + new Buffer(moduleConfig.login + ':' + moduleConfig.password).toString('base64')
    }
  }, function(res) {
    var body = '';
    res.on('data', function(chunk) {
      body += chunk;
    });

    res.on('end', function() {
      if (!body) {
        return cb(null);
      }

      var resBody = JSON.parse(body);
      cb(null, resBody);
    });
  });

  if (reqBody) { req.write(JSON.stringify(reqBody)); }
  req.end();
}

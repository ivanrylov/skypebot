'use strict';

module.exports = function(skypeApi, arg, message) {
  skypeApi.writeResponse(message, '<i>-> pong</i>', function(err) {
    if (err) { console.log(err); }
  });
};
